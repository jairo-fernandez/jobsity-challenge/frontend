import { getEntries, login, register, getAuthor } from '../services';
import { FETCH_ENTRIES, FETCH_TOKEN, FAIL_TOKEN, LOGOUT, FETCH_AUTHOR } from '../types';

export function fetchEntries(page) {
  return function(dispatch) {
    return getEntries(page)
      .then(( response ) => {
      dispatch(setEntry(response.data));
    });
  };
}

export function fetchAuthor(userId) {
  return function(dispatch) {
    return getAuthor(userId)
      .then(( response ) => {
        dispatch(setAuthor(response.data));
      });
  };
}

export function loginAction(data) {
  return function(dispatch) {
    return login(data)
      .then(( response ) => {
        dispatch(setToken(response.data, data.username));
      }).catch(err => {
        console.log(err);
        dispatch(failToken());
      });
  };
}

export function registerAction(data, history) {
  return function(dispatch) {
    return register(data)
      .then(( response ) => {
        if(!response) {
          throw Error('Error in register');
        }
        history.push('/login');
      }).catch(err => {
        console.log(err);
        alert(err);
      });
  };
}

export const setEntry = data => ({
  type: FETCH_ENTRIES,
  payload: data
});

export const setAuthor = data => ({
  type: FETCH_AUTHOR,
  payload: data
});

export const setToken= (data, username) => ({
  type: FETCH_TOKEN,
  payload: { token: data.token, username }
});

export const failToken= () => ({
  type: FAIL_TOKEN
});

export const logoutAction = () => ({
  type: LOGOUT
});
