import React from 'react';
import {Switch, Route} from 'react-router-dom';
import './App.scss';
import NotFound from './components/NotFound';
import {Home, Profile, Entry} from './pages';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import NavBar from './components/NavBar';
import Login from './pages/Login';
import Register from './pages/Register';
import Author from "./pages/Author";

function App() {
  return (
    <>
      <NavBar/>
      <Container>
        <CssBaseline/>
        <Container maxWidth="lg">
          <Switch>
            <Route exact path="/" component={Home}/>
            <Route exact path="/login" component={Login}/>
            <Route exact path="/register" component={Register}/>
            <Route exact path="/author/:username/:id" component={Author}/>
            <Route path="/profile" component={Profile}/>
            <Route path="/entry/:name" component={Entry}/>
            <Route component={NotFound}/>
          </Switch>
        </Container>
        <footer>
          <p>PHP Challenge Jobsity -- Jairo Fernández</p>
        </footer>
      </Container>
    </>
  );
}

export default App;
