import React from "react";
import CircularProgress from "@material-ui/core/CircularProgress";

const getCircularProgress = () => <div className={'circular__progress'}>
  <p>We are preparing all for you, please wait</p>
  <CircularProgress disableShrink size={200}/>
</div>;

export default getCircularProgress;
