import React, {Component} from 'react';
import Entry from './Entry';

class EntryList extends Component {
  render() {
    const { entries, redirectToAuthor} = this.props;
    if(!entries) return null;
    return <div className={'entries__container'}>{this.showList(entries, redirectToAuthor)}</div>;
  }

  showList(entries, redirectToAuthor) {
    return (entries.map(entry => {
      return <Entry key={entry.id} entry={entry} redirectToAuthor={redirectToAuthor}/>
    }));
  }
}

export default EntryList;
