// import React, { Component } from 'react';
//
// class Entry extends Component {
//   render() {
//     const { entry } = this.props;
//     return (
//       <article>
//         <h1>{entry.title}</h1>
//         <p>{entry.content}</p>
//         <small>@{entry.author.twitterUser.replace("@", "")}</small><br/>
//         <small>{entry.author.username}</small>
//       </article>
//     )
//   }
// }
//
// export default Entry;

import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles({
  card: {
    maxWidth: 345,
    margin: 10
  },
  media: {
    height: 140,
  },
});

export default function MediaCard({ entry, redirectToAuthor }) {
  const classes = useStyles();
  const random = Math.floor(Math.random() * 10);

  return (
    <Card className={classes.card}>
      <CardActionArea>
        <CardMedia
          className={classes.media}
          image={`https://picsum.photos/200/300?random=${random}`}
          title={`${entry.title}`}
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {entry.title}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            {entry.content.slice(0, 100)}...
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        {entry.author && <Button size="small" color="primary" onClick={() => redirectToAuthor(entry.author)}>
          <small>Author {entry.author.username}</small>
        </Button>}
        <Button size="small" color="primary">
          Read More
        </Button>
      </CardActions>
    </Card>
  );
}
