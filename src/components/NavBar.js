import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import {logoutAction} from "../actions";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  link: {
    color: 'white',
    textDecoration: 'none',
    fontSize: 16,
    marginRight: 10
  }
}));

function NavBar({ user, logout }) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" className={classes.title}>
            Jobsity challenge
          </Typography>
          <Link to={'/'} className={classes.link}>HOME</Link>
          {user.username ?
            <>
              <Link to={'/profile'} className={classes.link}>Hi {user.username.toUpperCase()}</Link>
              <Link to={''} onClick={() => logout()} className={classes.link}>Logout</Link>
            </>:
            <>
              <Link to={'/login'} className={classes.link}>LOGIN</Link>
              <Link to={'/register'} className={classes.link}>REGISTER</Link>
            </>
          }
        </Toolbar>
      </AppBar>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    user: state.user
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    logout: () => dispatch(logoutAction())
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(NavBar);
