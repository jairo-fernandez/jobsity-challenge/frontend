import axios from 'axios';

const REACT_APP_API_URL = process.env.REACT_APP_API_URL || 'https://jairo-fernandez-jobsity.herokuapp.com';

async function getEntries(page = 1, itemPerPage = 3) {
  try {
    return await axios.get(`${REACT_APP_API_URL}/api/entries?order[creationDate]=desc&page=${page}&itemsPerPage=${itemPerPage}`, {
      crossdomain: true,
      Accept: 'application/json'
    });
  } catch (error) {
    console.log(error);
  }
}

async function getAuthor(userId) {
  try {
    return await axios.get(`${REACT_APP_API_URL}/api/users/${userId}`, {
      crossdomain: true,
      Accept: 'application/json'
    });
  } catch (error) {
    console.log(error);
  }
}

async function login(data) {
  try {
    return await axios.post(`${REACT_APP_API_URL}/login`, data, {
      headers: {
        Accept: 'application/json'
      }
    });
  } catch (error) {
    console.log(error);
  }
}

async function register(data) {
  const user = `username=${data.username}&email=${data.email}&password=${data.password}&twitterUser=${data.twitterUser}`;
  try {
    return await axios.post(`${REACT_APP_API_URL}/register?${user}`, {}, {
      headers: {
        Accept: 'application/json'
      }
    });
  } catch (error) {
    console.log(error);
  }
}

export {
  getEntries,
  login,
  register,
  getAuthor
}
