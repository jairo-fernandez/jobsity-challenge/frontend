import { FETCH_ENTRIES } from '../types';

const entries = (state = [], action) => {
  switch (action.type) {
    case FETCH_ENTRIES:
      return {
        ...state,
        ...action.payload
      };
    default:
      return state
  }
};

export default entries;
