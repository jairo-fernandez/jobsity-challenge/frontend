import { combineReducers } from 'redux'
import entries from './entries'
import user from "./user";
import author from "./author";

export default combineReducers({
  entries,
  user,
  author
})
