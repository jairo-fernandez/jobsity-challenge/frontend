import {FAIL_TOKEN, FETCH_TOKEN, LOGOUT} from '../types';

const user = (state = [], action) => {
  switch (action.type) {
    case FETCH_TOKEN:
      return {
        ...action.payload
      };
    case FAIL_TOKEN:
      return {
        username: '',
        token: 'ERROR'
      };
    case LOGOUT:
      return {
        username: '',
        token: ''
      };
    default:
      return state
  }
};

export default user;
