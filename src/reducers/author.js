import { FETCH_AUTHOR } from '../types';

const author = (state = [], action) => {
  switch (action.type) {
    case FETCH_AUTHOR:
      return {
        ...state,
        ...action.payload
      };
    default:
      return state;
  }
};

export default author;
