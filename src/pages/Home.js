import React, {Component} from 'react';
import {connect} from 'react-redux';
import {fetchEntries} from '../actions';
import EntryList from "../components/entry/List";
import Button from "@material-ui/core/Button";
import getCircularProgress from "../components/entry/LoadingApp";

class Home extends Component {
  state = {
    activePagination: 1
  };

  componentDidMount() {
    this.getEntries();
  }

  getEntries = (page = 1) => {
    this.props.getEntries(page);
    this.setState({activePagination: page});
  };

  pagination(pagination) {
    const lastPageArray = pagination ? pagination['hydra:last'].split('=') : [];
    const lastPage = lastPageArray[lastPageArray.length - 1];
    const data = [];
    for (let i = 1; i <= lastPage; i++) {
      data.push(i);
    }

    return (
      <div className="home">
        <div className="pagination">
          {data.map(index =>
            <Button
              key={`pagination-${index}`}
              variant="outlined"
              href={`#pagination-${index}`}
              onClick={() => this.getEntries(index)}
              className={this.state.activePagination === index ? 'active' : ''}
            >
              {index}
            </Button>
          )}
        </div>
      </div>);
  }

  redirectToAuthor = (user) => {
    const userArray = user['@id'].split('/');
    const id = userArray[userArray.length - 1];
    this.props.history.push(`/author/${user.username}/${id}`)
  };

  render() {
    const {entries, pagination} = this.props;
    return (
      <>
        <h1>Home</h1>
        {this.pagination(pagination)}
        {!entries ? getCircularProgress() : <EntryList entries={entries} redirectToAuthor={this.redirectToAuthor}/>}
      </>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    entries: state.entries['hydra:member'],
    totalEntries: state.entries['hydra:totalItems'],
    pagination: state.entries['hydra:view']
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    getEntries: (page) => dispatch(fetchEntries(page))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
