import React, { Component} from 'react';
import {fetchAuthor} from "../actions";
import {connect} from "react-redux";
import EntryList from "../components/entry/List";
import getCircularProgress from "../components/entry/LoadingApp";

class Author extends Component {
  componentDidMount() {
    this.props.getAuthorData(this.props.match.params.id);
  }

  redirectToAuthor = (user) => {
    const userArray = user['@id'].split('/');
    const id = userArray[userArray.length - 1];
    this.props.history.push(`/author/${user.username}/${id}`)
  };

  render() {
    const {params} = this.props.match;
    const { entries } = this.props.author;
    if(!Array.isArray(entries)) return getCircularProgress();
    return(
      <>
        <h1>{params.username.toUpperCase()}</h1>
        <h2>Entries </h2>
        {!entries && !this.props.author ? getCircularProgress() : <EntryList entries={entries} redirectToAuthor={this.redirectToAuthor}/>}
      </>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    author: state.author
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    getAuthorData: (userId) => dispatch(fetchAuthor(userId))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Author);
