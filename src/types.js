export const FETCH_ENTRIES = "FETCH_ENTRIES";
export const FETCH_TOKEN = "FETCH_TOKEN";
export const FAIL_TOKEN = "FAIL_TOKEN";
export const LOGOUT = "LOGOUT";
export const FETCH_AUTHOR = "FETCH_AUTHOR";
