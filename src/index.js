import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import './index.css';
import App from './App';
import { Provider } from 'react-redux';
import * as serviceWorker from './serviceWorker';
import createStore from './store';

const store = createStore();
store.subscribe(() => {
  localStorage.setItem('jairo-jobsity', JSON.stringify(store.getState()));
});

const render = Component => {
  return ReactDOM.render(
    <Provider store={store}>
      <Router basename='/'>
        <Component />
      </Router>
    </Provider>,
    document.getElementById('root')
  );
};

render(App);

if (module.hot) {
  module.hot.accept('./App', () => {
    const NextApp = require('./App').default;
    render(NextApp);
  });
}

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
